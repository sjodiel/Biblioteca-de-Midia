/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author sjodiel
 */
public class CD extends Midia {

    // atributos
    protected int numDeFaixa;
    protected String artista;
    protected String album;

    public CD(int numDeFaixa, String artista, String album, Long number, String titulo, String genero, int ano, float duracao, String idioma, String comentario) {
        super(number, titulo, genero, ano, duracao, idioma, comentario);
        this.numDeFaixa = numDeFaixa;
        this.artista = artista;
        this.album = album;
    }

    /**
     * @return the numDeFaixa
     */
    public int getNumDeFaixa() {
        return numDeFaixa;
    }

    /**
     * @param numDeFaixa the numDeFaixa to set
     */
    public void setNumDeFaixa(int numDeFaixa) {
        this.numDeFaixa = numDeFaixa;
    }

    /**
     * @return the artista
     */
    public String getArtista() {
        return artista;
    }

    /**
     * @param artista the artista to set
     */
    public void setArtista(String artista) {
        this.artista = artista;
    }

    /**
     * @return the album
     */
    public String getAlbum() {
        return album;
    }

    /**
     * @param album the album to set
     */
    public void setAlbum(String album) {
        this.album = album;
    }
    
    @Override
    public String toString() {
    
        //return super.toString(); //To change body of generated methods, choose Tools | Templates.
        
        String valores = "Artista"+getArtista()+
                         ", album="+getAlbum()+
                         ", numero de faixa ="+getNumDeFaixa();
        
        return super.toString() + valores;
            
    }  
    
    
}

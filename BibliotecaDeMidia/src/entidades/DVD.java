/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author sjodiel
 */
public class DVD extends Midia {
    
    protected String diretor;
    //protected String estudio;
    protected String protagonista;
    protected int classificacao;

    public DVD(String diretor, String protagonista, int classificacao, Long number, String titulo, String genero, int ano, float duracao, String idioma, String comentario) {
        super(number, titulo, genero, ano, duracao, idioma, comentario);
        this.diretor = diretor;
        this.protagonista = protagonista;
        this.classificacao = classificacao;
    }

    /**
     * @return the diretor
     */
    public String getDiretor() {
        return diretor;
    }

    /**
     * @param diretor the diretor to set
     */
    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    /**
     * @return the protagonista
     */
    public String getProtagonista() {
        return protagonista;
    }

    /**
     * @param protagonista the protagonista to set
     */
    public void setProtagonista(String protagonista) {
        this.protagonista = protagonista;
    }

    /**
     * @return the classificacao
     */
    public int getClassificacao() {
        return classificacao;
    }

    /**
     * @param classificacao the classificacao to set
     */
    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }
    
    @Override
    public String toString() {
        String valores = " Codigo="+getNumber()+
                         ", titulo="+getTitulo()+
                         ", genero="+getGenero()+
                         ", ano="+getAno()+
                         ", duracao="+getDuracao()+
                         ", idioma="+getIdioma()+
                         ", comentário="+getComentario();
        
        return valores;
    }
    
}

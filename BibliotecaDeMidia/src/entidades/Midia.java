/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author sjodiel
 */
public class Midia {
    
     protected Long number;
     protected String titulo;
     protected String genero;
     protected int ano;
     protected float duracao;
     protected String idioma;
     protected String comentario;

    public Midia(Long number, String titulo, String genero, int ano, float duracao, String idioma, String comentario) {
        this.number = number;
        this.titulo = titulo;
        this.genero = genero;
        this.ano = ano;
        this.duracao = duracao;
        this.idioma = idioma;
        this.comentario = comentario;
    }
     

    /**
     * @return the number
     */
    public Long getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Long number) {
        this.number = number;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the duracao
     */
    public float getDuracao() {
        return duracao;
    }

    /**
     * @param duracao the duracao to set
     */
    public void setDuracao(float duracao) {
        this.duracao = duracao;
    }

    /**
     * @return the idioma
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the comentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    @Override
    public String toString() {
        String valores = " Codigo="+getNumber()+
                         ", titulo="+getTitulo()+
                         ", genero="+getGenero()+
                         ", ano="+getAno()+
                         ", duracao="+getDuracao()+
                         ", idioma="+getIdioma()+
                         ", comentário="+getComentario();
        
        return valores;
    }
    
}

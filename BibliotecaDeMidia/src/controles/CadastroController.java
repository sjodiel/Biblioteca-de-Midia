/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controles;

import entidades.CD;
import entidades.DVD;
//import entidades.Midia;

/**
 *
 * @author sjodiel
 */
public class CadastroController {
    
    // singleton
        
    
    public boolean cadastrarCD(CD p) {
     
        boolean result = false;
        
        
        if (p != null  &&  p.getTitulo().length() > 0 && p.getGenero().length() > 0
                &&  p.getAno() != 0 &&  p.getDuracao() != 0.0f &&  p.getIdioma().length() > 0
                &&  p.getComentario().length() > 0 &&  p.getNumDeFaixa() !=0  
                &&  p.getArtista().length() > 0 &&  p.getAlbum().length() > 0) {
         
                // seta ordem/number valido
                p.setNumber(RepositoriosManager.getInstance().getContadorMidia());
                
                // insere no modelo de dados
                RepositoriosManager.getInstance().inserirCD(p);
                
                System.out.println(p.toString());
                
                result = true;
           
        }
        
        
        return result;
    }
    

    public boolean atualizarCD(CD p) {
        
        
        boolean result = false;
        
        if (p != null  &&  p.getTitulo().length() > 0 && p.getGenero().length() > 0
                &&  p.getAno() !=0 &&  p.getDuracao() !=0.0f &&  p.getIdioma().length() > 0
                &&  p.getComentario().length() > 0 &&  p.getNumDeFaixa() !=0 
                &&  p.getArtista().length() > 0 &&  p.getAlbum().length() > 0) {
            
            
                // NOVO CODIGO
                RepositoriosManager.getInstance().modificarCD(p);
                System.out.println("UPDATED -> " + p.toString());
                result = true;
            
        }
        
        
        return result;
        
        
    }
    
public boolean cadastrarDVD(DVD d) {
     
        boolean result = false;
        
        if (d != null && d.getTitulo().length() > 0 && d.getGenero().length() > 0
                &&  d.getAno() !=0 &&  d.getDuracao() !=0.0f &&  d.getIdioma().length() > 0
                &&  d.getComentario().length() > 0 &&  d.getProtagonista().length() >0 
                &&  d.getDiretor().length() > 0 &&  d.getClassificacao() != 0) {
               // FAZER ALTERAÇÃO 
           
                // seta codigo valido
                d.setNumber(RepositoriosManager.getInstance().getContadorMidia());
                
                // insere no modelo de dados
                RepositoriosManager.getInstance().inserirDVD(d);
                
                System.out.println(d.toString());
                
                result = true;
            
        }
        
        
        return result;
    }


  public boolean atualizarDVD(DVD d) {
        
        
        boolean result = false;
        
        if (d != null && d.getTitulo().length() > 0 && d.getGenero().length() > 0
                &&  d.getAno() !=0 &&  d.getDuracao() !=0.0f &&  d.getIdioma().length() > 0
                &&  d.getComentario().length() > 0 &&  d.getProtagonista().length() >0 
                &&  d.getDiretor().length() > 0 &&  d.getClassificacao() != 0) {
            
            
            
                // NOVO CODIGO
                RepositoriosManager.getInstance().modificarDVD(d);
                System.out.println("UPDATED -> " + d.toString());
                result = true;
            
        }
        
        
        return result;
        
        
    }
  
    /*public boolean apagarCD(CD p) {
        
        
        boolean result = false;
        
        if (p != null  &&  p.getTitulo().length() > 0 && p.getGenero().length() > 0
                &&  p.getAno() !=0 &&  p.getDuracao() !=0.0f &&  p.getIdioma().length() > 0
                &&  p.getComentario().length() > 0 &&  p.getNumDeFaixa() !=0 
                &&  p.getArtista().length() > 0 &&  p.getAlbum().length() > 0) {
            
            
                // NOVO CODIGO
                RepositoriosManager.getInstance().aCD(p);
                System.out.println("apagando -> " + p.toString());
                result = true;
            
        }
        
        
        return result;
        
        
    }*/
    
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controles;

import entidades.CD;
import entidades.DVD;
import java.util.ArrayList;

/**
 *
 * @author sjodiel
 */
public class RepositoriosManager {
    
    // metodos serao acessado usando padrao SINGLETON
    private static RepositoriosManager instancia;
    
    // contador de midia
    private long contadorMidia = 1;

    
    // guarda os dados da midia
    private  ArrayList<CD> listaCD;
    private ArrayList<DVD> listaDVD;
    
    
    // retorna a instancia do repositorio manager
    public static RepositoriosManager getInstance() {
        
        if (instancia == null) {
            
            instancia = new RepositoriosManager();
        }
        
        return instancia;
    }

    // construtor private
    private RepositoriosManager() {
        
        listaDVD = new ArrayList<DVD>();
        listaCD = new ArrayList<CD>();
  
        //loadMidiaParaTestes();
        
    }
    
    
    /**
     * Usado para obter o contador atual
     * @return o numero atual do contador
     */
    public long getContadorMidia() {
        return contadorMidia;
    }
    
    
    
    public void inserirCD(CD p) {
        
        listaCD.add(p);
        
        contadorMidia++; // atualizar contador de midia
        
    }
    
    public void inserirDVD(DVD d) {
        
        listaDVD.add(d);
        
       contadorMidia++; // atualizar contador de midia
        
    }
    
    
    public ArrayList<CD> obterListaCD() {
        
        return listaCD;
    }
    
    public ArrayList<DVD> obterListaDVD() {
        
        return listaDVD;
    }
     

    /**
     * Cadastra dados FAKE para testes
     */
    private void loadMidiaParaTestes() {
        
        for (int i = 0; i < 20; i++) {
            //CD p = new CD(
              //      new Long(contadorMidia), // codigo 
                //    "N "+(i+1),  // nome do album
                  //  "...", // comentário
                    //1990, // ano
                    //20, // faix
                    //rock); // gerenro
            
            
            //contadorMidia++;
            //listaCD.add(p);
        }
        
        
    }

    /**
     * Altera a midia.
     * 
     * @param p o cd a ser alterado 
     */
    public void modificarCD(CD p) {
        
        if (listaCD.contains(p)) {
            
            int index = listaCD.indexOf(p);
            listaCD.set(index, p);
            
        }
        
    }
        
    /**
     * Altera a midia.
     * 
     * @param d o dvd a ser alterado 
     */
    
    public void modificarDVD(DVD d) {
        
        if (listaDVD.contains(d)) {
            
            int index = listaDVD.indexOf(d);
            listaDVD.set(index, d);
            //listaDVD.clear();
            
        }
        
    }    
    
    public void apagarCD(CD p){
        
            //contadorMidia--;
            listaCD.remove(p);
            contadorMidia--;
           
            

    }
    
    public void apagarDVD(DVD d){
        
            
            listaDVD.remove(d);
            contadorMidia--;
            
            

    }
}